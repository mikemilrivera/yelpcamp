// require the packages
const express = require("express"),
			bodyParser = require("body-parser"),
			mongoose = require("mongoose"),
			passport = require("passport"),
			LocalStrategy = require("passport-local"),
			expressSession = require("express-session"),
			app = express(), // initialize the app
			port = process.env.PORT || 5000, // declare server port
			Campground = require("./models/Campground"),
			Comment = require("./models/Comment"),
			User = require("./models/User"),
			seedDB = require("./seeds");

const campgroundRoutes = require("./routes/campgrounds"),
			commentRoutes = require("./routes/comments"),
			indexRoutes = require("./routes/index");

// seed the database
// seedDB();
mongoose.connect('mongodb://localhost:27017/yelp_camp', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useCreateIndex: true,
	useFindAndModify: true
})
.then(() => console.log("Connected to DB"))
.catch(error => console.log(error.message));

// declare ejs as defaul view file type
app.set("view engine", "ejs");
// setup body parser
app.use(bodyParser.urlencoded({extended: true}));
// serve public directory
app.use(express.static(__dirname + "/public"));

// PASSPORT CONFIGURATION
app.use(expressSession({
	secret: "Once again Rusty wins cutest dog!",
	resave: false,
	saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// MIDDLEWARE
// this middleware will be called in every route (app level)
app.use((req, res, next) => {
	res.locals.currentUser = req.user;
	next();
})

// ROUTES
app.use(indexRoutes);
app.use("/campgrounds", campgroundRoutes);
app.use("/campgrounds/:id/comments", commentRoutes);

// listener
app.listen(port, () => {
	console.log(`YelpCamp is listening to port ${port}.`);
});