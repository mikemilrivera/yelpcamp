const express = require("express"),
			router = express.Router(),
			Campground = require("../models/Campground");

// router level middleware
const isLoggedIn = (req, res, next) => {
	if(req.isAuthenticated()){
		return next();
	}
	res.redirect("/login");
}

// INDEX - show all campgrounds
router.get("/", (req, res) => {
	// get all campgrounds from DB
	Campground.find({}, (err, allCampgrounds) => {
		if(err){
			console.log(err);
		} else {
			res.render("campgrounds/index", {campgrounds: allCampgrounds});
		}
	});
});

// CREATE - add new campground to DB
router.post("/", isLoggedIn, (req, res) => {
	// get data from form and add to campgrounds array
	const name = req.body.name;
	const image = req.body.image;
	const description = req.body.description;
	const author = {
		id: req.user._id,
		username: req.user.username
	}
	const newCampground = {
		name: name, 
		image: image, 
		description: description,
		author: author
	};

	// create a new campground and save to DB
	Campground.create(newCampground, (err, newlyCreated) => {
		if(err){
			console.log(err);
		} else {
			console.log(newlyCreated);
			// redirect back to campgrounds page
			res.redirect("/campgrounds");
		}
	});
});

// NEW - show from to create new campground
router.get("/new", isLoggedIn, (req, res) => {
	res.render("campgrounds/new");
});

// SHOW - show more info about one campground
router.get("/:id", (req, res) => {
	// find the campground with provided ID
	Campground.findById(req.params.id).populate("comments").exec((err, foundCampground) => {
		if(err){
			console.log(err);
		} else {
			// render show template with that campground
			res.render("campgrounds/show", {campground: foundCampground});
		}
	});
});

module.exports = router;