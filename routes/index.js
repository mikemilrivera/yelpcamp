const express = require("express"),
			passport = require("passport"),
			router = express.Router(),
			User = require("../models/User");

// router level middleware
const isLoggedIn = (req, res, next) => {
	if(req.isAuthenticated()){
		return next();
	}
	res.redirect("/login");
}

// root route
router.get("/", (req, res) => {
	res.render("landing");
});

// show register form
router.get("/register", (req, res) => {
	res.render("register");
});

// handle sign up logic
router.post("/register", (req, res) => {
	const newUser = new User({username: req.body.username});
	const password = req.body.password;
	User.register(newUser, password, (err, user) => {
		if(err){
			console.log(err);
			return res.render("register");
		}
		passport.authenticate("local")(req, res, () => {
			res.redirect("/campgrounds");
		})
	});
});

// show login form
router.get("/login", (req, res) => {
	res.render("login");
})

// handling login logic
router.post("/login", passport.authenticate("local", {
	successRedirect: "/campgrounds",
	failureRedirect: "/login"
}), (req, res) => {
});

// logout route
router.get("/logout", (req, res) => {
	req.logout();
	res.redirect("/campgrounds");
})

module.exports = router;